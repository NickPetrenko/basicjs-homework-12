document.addEventListener("DOMContentLoaded", function() {
    var keyButtons = document.querySelectorAll('.key-button');
    var activeButton = null;
  
    document.addEventListener('keydown', function(event) {
      var key = event.key.toUpperCase();
      var targetButton = null;
  
      keyButtons.forEach(function(button) {
        if (button.dataset.key === key) {
          targetButton = button;
        }
      });
  
      if (targetButton) {
        if (activeButton) {
          activeButton.classList.remove('active');
          activeButton.classList.add('inactive');
        }
  
        targetButton.classList.remove('inactive');
        targetButton.classList.add('active');
        activeButton = targetButton;
      }
    });
});